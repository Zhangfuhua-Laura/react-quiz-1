import React from "react";
import ControlPanel from './ControlPanel';
import PlayingPanel from './PlayingPanel';

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            answer: []
        };
        this.setAnswer = this.setAnswer.bind(this);
    }

    render() {
        return (
            <div>
                <PlayingPanel answer={this.state.answer}/>
                <ControlPanel setAnswer={this.setAnswer} answer={this.state.answer}/>
            </div>
        );
    }

    setAnswer(answerResult){
        this.setState({answer: answerResult});
    }
}

export default App;
