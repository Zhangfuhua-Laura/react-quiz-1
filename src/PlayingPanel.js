import React from "react";
import YourResult from "./YourResult";
import GuessCard from "./GuessCard";

class PlayingPanel extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            yourResult: "",
            status: false
        }
        this.setYourResult = this.setYourResult.bind(this);
        this.judge = this.judge.bind(this);
    }

    render() {
        return (
            <div>
                <YourResult setYourResult={this.setYourResult} judge={this.judge}/>
                <GuessCard  status={this.state.status}/>
            </div>
        );
    }
    judge(){
        if(this.props.answer.toString() === this.state.yourResult){
            this.setState({status: true});
        }
        else {
            this.setState({status: false});
        }
    }
    setYourResult(yourResult){
        this.setState({yourResult: yourResult});
    }
}

export default PlayingPanel;