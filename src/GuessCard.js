import React from 'react';

class GuessCard extends React.Component{

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return(
            <div> {this.props.status? 'SUCCESS' : 'FAILED'}</div>
        );
    }
}
export default GuessCard;