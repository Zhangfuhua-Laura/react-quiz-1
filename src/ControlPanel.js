import React from "react";
import ShowAnswer from "./ShowAnswer";

class ControlPanel extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.generateAnswer = this.generateAnswer.bind(this);
    }

    render() {
        return (
            <div>
                <button onClick={this.generateAnswer}>New Game</button>
                <ShowAnswer answer={this.props.answer}/>
            </div>
        );
    }

    generateAnswer(){
        let answerResult = [];
        for(let i=0; i<4; i++)
            answerResult.push(ControlPanel.getRandomLetter());
        console.log(answerResult);

        this.props.setAnswer(answerResult);
    }

    static getRandomLetter(){
        let asciiA = 65;
        let asciiZ = 90;
        let randomAscii = Math.floor((Math.random()* (asciiZ-asciiA)) + asciiA);
        return String.fromCharCode(randomAscii);
    }
}

export default ControlPanel;