import React from 'react';

class ShowAnswer extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            appear: true
        }
        this.showResult = this.showResult.bind(this);
    }

    componentDidMount() {
        setTimeout(() => this.setState({appear: false}), 3000);
    }

    render() {
        return (
            <div id='answerShow'>
                {this.state.appear ? this.props.answer.toString() : ""}
                <button onClick={this.showResult}>Show</button>
            </div>
        );
    }

    showResult() {
        this.setState({appear: true});
    }
}

export default ShowAnswer;