import React from 'react';

class YourResult extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.setResult = this.setResult.bind(this);
    }

    render() {
        return(
            <form>
                <input type="text" id='answer'/>
                <button onClick={this.setResult}>Guess</button>
            </form>
        );
    }

    setResult(event) {
        event.preventDefault();

        let answer = document.getElementById('answer').value;
        this.props.setYourResult(answer);
        this.props.judge();
    }
}
export default YourResult;